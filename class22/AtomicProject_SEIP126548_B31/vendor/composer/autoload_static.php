<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit7ba649434e99a4719bab324d31c31fa4
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/Src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit7ba649434e99a4719bab324d31c31fa4::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit7ba649434e99a4719bab324d31c31fa4::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
