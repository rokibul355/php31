<?php
namespace App\Bitm\SEIP126548\Mobile;
class Mobile
{
    public $id = "";
    public $title = "";
    public $created = "";
    public $modified = "";
    public $created_by = "";
    public $modified_by = "";
    public $deleted_at = "";

    public function index()
    {
        return "I am listing data";
    }

    public function create()
    {
        return "I am create form<br>";
    }

    public function store()
    {
        return "I am storing data<br>";
    }

    public function edit()
    {
        return "I am editing form<br>";
    }

    public function update()
    {
        return "I am updating data<br>";
    }

    public function delete()
    {
        return "I delete data";
    }
   public function __construct($model = false)
   {
       echo "I am constructing data<br>";
   }
}
?>