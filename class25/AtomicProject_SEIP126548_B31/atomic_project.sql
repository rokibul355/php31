-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2016 at 07:53 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `email_addresses`
--

CREATE TABLE `email_addresses` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_addresses`
--

INSERT INTO `email_addresses` (`id`, `name`, `email`) VALUES
(1, 'Shakil Ahmed', 'shakiltct@yahoo.com'),
(2, 'Rokibul Islam Rabbi', 'rokibultct@gmail.com'),
(3, 'Ratul Islam Antor', 'ratulislam65@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `new_hobbies`
--

CREATE TABLE `new_hobbies` (
  `id` int(11) NOT NULL,
  `hobbies` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `new_hobbies`
--

INSERT INTO `new_hobbies` (`id`, `hobbies`) VALUES
(2, 'Cricket'),
(3, 'Football'),
(4, 'Baseball'),
(5, 'Writing'),
(6, 'Racing'),
(7, 'Fighting'),
(8, 'Walking'),
(10, 'Watching Movie'),
(11, 'Natok'),
(12, 'Fishing'),
(13, 'Dancing'),
(14, 'Teaching'),
(15, 'Boxing'),
(16, 'Eating'),
(17, 'Shopping'),
(18, 'Wrestling'),
(19, 'Coding'),
(20, 'Gardening'),
(21, 'Driving'),
(22, 'Thinking'),
(23, 'Cooking'),
(24, 'Chess');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `model_name` varchar(255) NOT NULL,
  `deleted_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `title`, `model_name`, `deleted_at`) VALUES
(3, 'Symphony', 'W71', NULL),
(4, 'Symphony', 'D54i', NULL),
(5, 'Nokia', '1100', NULL),
(6, 'Samsung', 'j5', '13-Nov-2016 12:50:25 PM'),
(12, 'Nokia', '25', NULL),
(13, 'Hawuai', '64', '13-Nov-2016 12:50:23 PM'),
(14, 'Nokia', '2098', NULL),
(15, 'Symphony', 'r5', NULL),
(16, 'Nokia', '1200', NULL),
(17, 'Symphony', 'z5', '13-Nov-2016 12:50:26 PM'),
(18, 'Symphony', 'w75', '13-Nov-2016 12:48:39 PM');

-- --------------------------------------------------------

--
-- Table structure for table `user_hobbies`
--

CREATE TABLE `user_hobbies` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `hobbies` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_hobbies`
--

INSERT INTO `user_hobbies` (`id`, `name`, `hobbies`) VALUES
(1, 'Shakil Ahmed', 'Coding,Wrestling'),
(4, 'shajib', 'Football,Baseball'),
(5, 'Rashed', 'Wrestling,Racing'),
(6, 'Raju', 'Fighting,Watching Movie,Boxing,Eating'),
(7, 'Rabbi', 'Fighting,Watching Movie,Boxing,Shopping,Wrestling'),
(8, 'Kader', 'Fishing'),
(9, 'Ratul Islam Antor', 'Gardening,Teaching,Watching Movie'),
(11, 'Bappy', 'Thinking,Driving');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `email_addresses`
--
ALTER TABLE `email_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `new_hobbies`
--
ALTER TABLE `new_hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_hobbies`
--
ALTER TABLE `user_hobbies`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `email_addresses`
--
ALTER TABLE `email_addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `new_hobbies`
--
ALTER TABLE `new_hobbies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `user_hobbies`
--
ALTER TABLE `user_hobbies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
