<?php
session_start();
include("../../../vendor/autoload.php");
use App\Bitm\SEIP126548\Hobby\Hobby;
use App\Bitm\SEIP126548\Message\Message;
$obj = new Hobby();
$all_result = $obj->showHobby();
$obj->setData($_GET);
$data = $obj->view();
$data['hobbies'] = explode(",", $data['hobbies']);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Edit Hobbies</title>

    <!-- Bootstrap -->
    <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <h2>Edit Hobbies</h2>
        </div>
        <div class="col-md-2"></div>
    </div>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8" id="message">
            <?php
            if(array_key_exists("message", $_SESSION) && !empty($_SESSION)){
                echo Message::message();
            }
            ?>
        </div>
        <div class="col-md-2"></div>
    </div>
    <form action="update.php" method="post">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8 form-group">
				<input type="hidden" name="id" value="<?php echo $data['id'];?>" />
                <label for="name">Name : </label>
                <input type="text" id="name" name="name" value="<?php echo $data['name'];?>" class="form-control" placeholder="Enter your name" required="required"/>
            </div>
            <div class="col-md-2"></div>
        </div>
		
        <div class="col-md-3"></div>
        
        
		<div class="col-md-6" style="overflow-y:scroll; overflow-x:hidden; border:1px solid #CCCCCC; height:300px">
		<?php 
		$i = 0;
		foreach($all_result as $result){
			$i++;
		
		?>
		<div class="row ">
            <div class="col-md-4"></div>
            
			<div class="col-md-4">
				
				<label class="checkbox-inline" for=""><input type="checkbox" id="check" name="check[]" <?php if(in_array($result['hobbies'], $data['hobbies'])){echo "checked";}?> value="<?php echo $result['hobbies'];?>"/><?php echo $result['hobbies'];?></label>
				
			</div>
			
            <div class="col-md-4"></div>
        </div>
		<?php 
		}
		?>
		
		</div>
		<div class="col-md-3"></div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8 col-md-offset-9">
                <input type="submit" class="btn btn-default" value="Update"/>
            </div>
            <div class="col-md-2"></div>
        </div>
    </form>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
    $("#message").show().delay(3000).fadeOut();
</script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../../bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
