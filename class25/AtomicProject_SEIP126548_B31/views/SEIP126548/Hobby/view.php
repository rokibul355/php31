<?php 
include_once("../../../vendor/autoload.php");
use App\Bitm\SEIP126548\Hobby\Hobby;
$obj = new Hobby();
$obj->setData($_GET);
$_result = $obj->view();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Hobby Project</title>

    <!-- Bootstrap -->
    <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <h3>Hobbies Information</h3>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
			<ul class="list-group">
				<li class="list-group-item">ID : <?php echo $_result['id']; ?></li>
				<li class="list-group-item">Name : <?php echo $_result['name']; ?></li>
				<li class="list-group-item">Hobbies : <?php echo $_result['hobbies']; ?></li>
			</ul>
		</div>
        <div class="col-md-3"></div>
    </div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script>
    $("#message").show().delay(3000).fadeOut();
</script>
<script src="../../../bootstrap/js/bootstrap.min.js"></script>
</body>
</html>