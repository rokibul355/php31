<?php
session_start();
include("../../../vendor/autoload.php");
use App\Bitm\SEIP126548\Mobile\Mobile;
use App\Bitm\SEIP126548\Message\Message;
$obj = new Mobile();
$result = $obj->index();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Mobile Project</title>

    <!-- Bootstrap -->
    <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<h3>List of Mobile </h3>
			</div>
			<div class="col-md-4"></div>
		</div>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6" id="message">
			<?php 
			if(array_key_exists("message", $_SESSION) && !empty($_SESSION['message'])){
			
				echo Message::message();
			}
			?>
			
			
			</div>
			<div class="col-md-3"></div>
		</div>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<a href="trashed.php" class="btn btn-info">Trashed Record</a>
				<a href="pdf.php" class="btn btn-primary">Download PDF</a>
				<a href="trashed.php" class="btn btn-warning">Download Excel</a>
				<a href="create.php" class="btn btn-success">Add Model</a><br><br>
			</div>
			<div class="col-md-1"></div>
		</div>
		
		
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<table class="table table-bordered">
					<thead>
						<tr class="success">
							<th>SL</th>
							<th>ID</th>
							<th>Brand</th>
							<th>Model</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							$i = 0;
							foreach($result as $data){
							$i++;	
						?>
						<tr>
							<td class="warning"><?php echo $i; ?></td>
							<td class="warning"><?php echo $data['id']; ?></td>
							<td class="warning"><?php echo $data['title'];?></td>
							<td class="warning"><?php echo $data['model_name'];?></td>
							<td class="warning" width="28%">
								<a href="view.php?id=<?php echo $data['id']; ?>" class="btn btn-info">View</a>
								<a href="edit.php?id=<?php echo $data['id']; ?>" class="btn btn-primary">Edit</a>
								<a href="delete.php?id=<?php echo $data['id'];?>"  class="btn btn-danger"  onclick="return confirm_delete();">Delete</a>
								<a href="trash.php?id=<?php echo $data['id']; ?>" class="btn btn-warning">Trash</a>
							</td>
						</tr>
						<?php 
						}
						?>
					</tbody>
				</table>
			</div>
			<div class="col-md-1">
				

			</div>
		</div>
	</div>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script type="text/javascript">
	$("#message").show().delay(3000).fadeOut();

	function confirm_delete(){
		var ok = confirm("Are you sure want to delete this permanently ??");
		if(ok){
			return true;
		}else{
			return false;
		}
	}

	</script>
	<script src="../../../bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
