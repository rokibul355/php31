<?php 
include("../../../vendor/autoload.php");
include("../../../vendor/mpdf/mpdf/mpdf.php");
use App\Bitm\SEIP126548\Mobile\Mobile;
$obj = new Mobile();
$all_data = $obj->index();

$trows = "";
$i = 0;
foreach($all_data as $data){
	$i++;
    $trows.="<tr>";
    $trows.="<td> $i</td>";
    $trows.="<td> $data[id] </td>";
    $trows.="<td> $data[title]</td>";
    $trows.="<td> $data[model_name]</td>";
    $trows.="</tr>";
}
$html = <<<HDOC
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Mobile Project</title>

    <!-- Bootstrap -->
    <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<style type="text/css">
		th,td{text-align:center}
		
	</style>
</head>
<body>
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
			<table class="table table-bordered">
				<thead>
					<tr style="text-align:center">
						<th>SL</th>
						<th>ID</th>
						<th>Brand</th>
						<th>Model</th>
					</tr>
				</thead>
				<tbody>
					$trows;
				</tbody>
			</table>
		</div>
		<div class="col-md-1">
		</div>
	</div>
</body>
</html>	
HDOC;
$mpdf = new Mpdf();
$mpdf->WriteHTML($html);
$mpdf->Output();
?>