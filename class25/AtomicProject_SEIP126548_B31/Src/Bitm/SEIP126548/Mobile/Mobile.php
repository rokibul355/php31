<?php
namespace App\Bitm\SEIP126548\Mobile;
use App\Bitm\SEIP126548\Message\Message;
use PDO;
class Mobile
{
    public $id = "";
    public $title = "";
    public $model_name = "";
    public $created = "";
    public $modified = "";
    public $created_by = "";
    public $modified_by = "";
    public $deleted_at = "";
    public $connection = "";
    public $host = "localhost";
    public $db_name = "atomic_project";
    public $user = "root";
    public $pass = "";
	public $location = "";

    public function __construct()
    {
        try{
            $this->connection = new PDO("mysql:host=$this->host;dbname=$this->db_name", $this->user, $this->pass);
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }
    }
    public function setData($data){
        if(array_key_exists("title", $data) && !empty($data)){
            $this->title = $data['title'];
        }
        if(array_key_exists("model_name", $data) && !empty($data)){
            $this->model_name = $data['model_name'];
        }
        if(array_key_exists("id", $data) && !empty($data)){
            $this->id = $data['id'];
        }
		if(array_key_exists("location", $data) && !empty($data)){
            $this->location = $data['location'];
        }
        return $this;
    }
    public function store()
    {
		$result = $this->index();
		$duplicate = 0;
		foreach($result as $row){
			if($row['title'] == $this->title && $row['model_name'] == $this->model_name){
				$duplicate = 1;
			}
		}
		if($duplicate == 1){
			Message::message("<div class='alert alert-danger'>This model is already inserted.</div>");
			header("location: create.php");
		}else{
        $statement = $this->connection->prepare("insert into student (title, model_name) values (:title, :model)");
        $result = $statement->execute(array("title"=>$this->title, "model"=>$this->model_name));
		if($result){
			Message::message("<div class='alert alert-success'>Data has been inserted successfully.</div>");
			header("location: index.php");
		}else{
            Message::message("<div class='alert alert-danger'>Data has not been inserted successfully.</div>");
            header("location: create.php");
        }
	}
    }
    public function index()
    {
        $statement = $this->connection->query("select * from atomic_project.student where deleted_at is null");
		$_all_data = $statement->fetchAll(PDO::FETCH_ASSOC);
		return $_all_data;
    }

    public function view(){
        $statement = $this->connection->prepare("select * from student where id=:id");
        $statement->execute(array("id"=>$this->id));
        $single_data = $statement->fetch(PDO::FETCH_ASSOC);
        return $single_data;
    }

    public function update()
    {
        $all_data = $this->index();
        $duplicate = 0;
        foreach($all_data as $data){
            if($data['title']== $this->title && $data['model_name'] == $this->model_name){
                $duplicate = 1;
            }
        }
        if($duplicate == 1){
            Message::message("<div class='alert alert-danger'>Data already in the database </div>");
            header("location: index.php");
        }else{
            $statement = $this->connection->prepare("update student set title= :title, model_name = :model where id = :id");
            $result = $statement->execute(array("title" =>$this->title, "model" => $this->model_name, "id" => $this->id));
            if($result){
                Message::message("<div class='alert alert-success'>Data has been updated successfully! </div>");
                header("location: index.php");
            }else{
                Message::message("<div class='alert alert-danger'>Data has not been updated !!</div>");
                header("location: edit.php");
            }
        }
    }

    public function delete()
    {
        $statement = $this->connection->prepare("delete from student where id = :id");
        $result = $statement->execute(array("id" => $this->id));
        if($result){
            Message::message("<div class='alert alert-success'>Data has been deleted successfully </div>");
			if($this->location=="trashed.php"){
				header("location: trashed.php");
			}else{ 
				header("location: index.php");
			}
        }else{
            Message::message("<div class='alert alert-danger'>Data has not been deleted !!</div>");
				header("location: index.php");
		}
    }

    public function trash(){
        date_default_timezone_set("Asia/Dhaka");
        $this->deleted_at = date("d-M-Y h:i:s A");
        $statement = $this->connection->prepare("update student set deleted_at = :date where id = :id");
        $result = $statement->execute(array("date" => $this->deleted_at, "id" => $this->id));
        if($result){
            Message::message("<div class='alert alert-success'>Data has been trashed successfully </div>");
            header("location:index.php");
        }else{
            Message::message("<div class='alert alert-danger'>Data has not been trased!!</div>");
            header("location: index.php");
        }
    }
    public function trashedView()
    {
        $statement = $this->connection->query("select * from student where deleted_at is not null");
        $_all_data = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $_all_data;
    }


    public function restore(){
        $this->deleted_at = null;
        $statement = $this->connection->prepare("update student set deleted_at = :date where id = :id");
        $result = $statement->execute(array("date" => $this->deleted_at, "id" => $this->id));
        if($result){
            Message::message("<div class='alert alert-success'>Data has been restored successfully!</div>");
            header("location:trashed.php");
        }else{
            Message::message("<div class='alert alert-danger'>Data has not been restored</div>");
            header("location:trashed.php");
        }
    }



}
?>