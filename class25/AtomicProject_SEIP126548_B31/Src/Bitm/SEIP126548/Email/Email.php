<?php
namespace App\Bitm\SEIP126548\Email;
use PDO;
use App\Bitm\SEIP126548\Message\Message;
class Email{
    public $id = "";
    public $name = "";
	public $email = "";
	public $connection = "";
	public $host = "localhost";
	public $db_name = "atomic_project";
	public $user = "root";
	public $pass = "";
	
	public function __construct(){
		try{
			$this->connection = new PDO("mysql:host=$this->host;dbname=$this->db_name", $this->user, $this->pass);
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
	}
	public function setData($data){
		if(array_key_exists("name", $data) && !empty($data)){
			$this->name = $data['name'];
		}
		if(array_key_exists("email", $data) && !empty($data)){
			$this->email = $data['email'];
		}
		if(array_key_exists("id", $data) && !empty($data)){
			$this->id = $data['id'];
		}
		return $this;
	}
	public function store(){
		$result = $this->index();
		$duplicate = 0;
		foreach($result as $row){
			if($row['email'] == $this->email){
				$duplicate = 1;
			}
		}
		if($duplicate == 1){
			Message::message("<div class='alert alert-danger'>This email is already in the database</div>");
			header("location: create.php");
		}
		else{
			$statement = $this->connection->prepare("insert into email_addresses (name, email) values (:name, :email)");
			$result = $statement->execute(array("name"=>$this->name, "email"=>$this->email));
			if($result){
				Message::message("<div class='alert alert-success'>Data has been stored successfully.</div>");
				header("location: index.php");
			}else{
				Message::message("<div class='alert alert-danger'>Data has not been stored successfully.</div>");
				header("location: create.php");
			}
		}
	}
	public function index(){
		$statement = $this->connection->query("select * from email_addresses");
		$_all_data = $statement->fetchAll(PDO::FETCH_ASSOC);
		return $_all_data;
	}
	public function view(){
		$statement = $this->connection->prepare("select * from email_addresses where id=:id");
		$statement->execute(array("id"=>$this->id));
		$_result = $statement->fetch(PDO::FETCH_ASSOC);
		return $_result;
	}
	public function update(){
		$statement = $this->connection->prepare("update email_addresses set name = :name, email = :email where id = :id");
		$result = $statement->execute(array("name" => $this->name, "email" => $this->email, "id" => $this->id));
		if($result){
			Message::message("<div class='alert alert-success'>Data has been updated successfully</div>");
			header("location: index.php");
		}else{
			Message::message("<div class='alert alert-danger'>Data has not been updated successfully </div>");
			header("location: index.php");
		}
	}
	public function delete(){
		$statement = $this->connection->prepare("delete from email_addresses where id = :id");
		$result = $statement->execute(array("id" => $this->id));
		if($result){
			Message::message("<div class='alert alert-success'>Data has been deleted successfully </div>");
			header("location: index.php");
		}else{ 
			Message::message("<div class='alert alert-danger'>Data has not been deleted successfully !!</div>");
			header("location: index.php");
		}
	}
}
?>