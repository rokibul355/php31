<?php
namespace App\Bitm\SEIP126548\Hobby;
use PDO;
use App\Bitm\SEIP126548\Message\Message;
class Hobby{
    public $id = "";
    public $name = "";
	public $hobbies = "";
	public $connection = "";
	public $deleted_at = "";
	public $host = "localhost";
	public $db_name = "atomic_project";
	public $user = "root";
	public $pass = "";
	
	public function __construct(){
		try{
			$this->connection = new PDO("mysql:host=$this->host;dbname=$this->db_name", $this->user, $this->pass);
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
	}
	public function setData($data){
		if(array_key_exists("name", $data) && !empty($data)){
			$this->name = $data['name'];
		}
		if(array_key_exists("check", $data) && !empty($data)){
			$this->hobbies = $data['check'];
		}
		if(array_key_exists("id", $data) && !empty($data)){
			$this->id = $data['id'];
		}
		return $this;
	}
	public function store(){
		$statement = $this->connection->prepare("insert into user_hobbies (name, hobbies) values (:name, :hobbies)");
		$result = $statement->execute(array("name"=>$this->name, "hobbies"=>$this->hobbies));
		if($result){
			Message::message("<div class='alert alert-success'>Data has been stored successfully.</div>");
			header("location: index.php");
		}else{
			Message::message("<div class='alert alert-danger'>Please select your hobbies from checkbox !</div>");
			header("location: create.php");
		}
	}
	public function index(){
		$statement = $this->connection->query("select * from user_hobbies where deleted_at is null");
		$_all_data = $statement->fetchAll(PDO::FETCH_ASSOC);
		return $_all_data;
	}
	public function view(){
		$statement = $this->connection->prepare("select * from user_hobbies where id=:id");
		$statement->execute(array("id"=>$this->id));
		$_result = $statement->fetch(PDO::FETCH_ASSOC);
		return $_result;
	}
	public function update(){
		$statement = $this->connection->prepare("update user_hobbies set name = :name, hobbies = :hobbies where id = :id");
		$result = $statement->execute(array("name" => $this->name, "hobbies" => $this->hobbies, "id" => $this->id));
		if($result){
			Message::message("<div class='alert alert-success'>Data has been updated successfully</div>");
			header("location: index.php");
		}else{
			Message::message("<div class='alert alert-danger'>Data has not been updated successfully </div>");
			header("location: index.php");
		}
	}
	public function delete(){
		$statement = $this->connection->prepare("delete from user_hobbies where id = :id");
		$result = $statement->execute(array("id" => $this->id));
		if($result){
			Message::message("<div class='alert alert-success'>Data has been deleted successfully </div>");
			header("location: index.php");
		}else{ 
			Message::message("<div class='alert alert-danger'>Data has not been deleted successfully </div>");
			header("location: index.php");
		}
	}
	public function storeHobby(){
		$all_hobbies = $this->showHobby();
		$name = strtolower($this->name);
		
		foreach($all_hobbies as $hobby){
			$hobby = strtolower($hobby['hobbies']);
			if($hobby == $name){
				$duplicate = 1;
			}
		}
		if($duplicate == 1){
			Message::message("<div class='alert alert-danger'>$this->name is already in the database !</div>");
			header("location: create.php");
		}else{
			$statement = $this->connection->prepare("insert into new_hobbies (hobbies) values (:hobbies)");
			$this->name = ucfirst($this->name);
			$result = $statement->execute(array("hobbies"=>$this->name));
			if($result){
				Message::message("<div class='alert alert-success'>Data has been stored successfully.</div>");
				header("location: create.php");
			}else{
				Message::message("<div class='alert alert-danger'>Data has not been stored successfully </div>");
				header("location: add_hobbies.php");
			}
		}
	}
	public function showHobby(){
		$statement = $this->connection->query("select * from new_hobbies order by id desc");
		$all_result = $statement->fetchAll(PDO::FETCH_ASSOC);
		return $all_result;
	}
	public function trash(){
		date_default_timezone_set("Asia/Dhaka");
		$this->deleted_at = date("d-m-Y h:i:s A");
		$statement = $this->connection->prepare("update user_hobbies set deleted_at = :date where id = :id");
		$result = $statement->execute(array("date" =>$this->deleted_at, "id" =>$this->id));
		if($result){
			Message::message("<div class='alert alert-success'>Data has been trashed successfully !</div>");
			header("location: index.php");
		}else{
			Message::message("<div class='alert alert-danger'>Data has not been trashed successfully !</div>");
			header("location: index.php");
		}
	}
	public function trashView(){
		$statement = $this->connection->query("select * from user_hobbies where deleted_at is not null");
		$all_data = $statement->fetchAll(PDO::FETCH_ASSOC);
		return $all_data;
	}
	public function restore(){
		$this->deleted_at = null;
		$statement = $this->connection->prepare("update user_hobbies set deleted_at = :date where id = :id");
		$result = $statement->execute(array("date" =>$this->deleted_at, "id" =>$this->id));
		if($result){
			Message::message("<div class='alert alert-success'>Data has been restored successfully!</div>");
			header("location: trashed.php");
		}else{
			Message::message("<div class='alert alert-danger'>Data has not been restored successfully!</div>");
			header("location: trashed.php");
		}
	}
}
?>