<?php
namespace App\Bitm\SEIP126548\Gender;
use PDO;
use App\Bitm\SEIP126548\Message\Message;
class Gender{
    public $id = "";
    public $name = "";
    public $gender = "";
    public $deleted_at = "";
    public $connection = "";
    public $host = "localhost";
    public $db_name = "atomic_project";
    public $user = "root";
    public $pass = "";

    public function __construct()
    {
        try{
            $this->connection = new PDO("mysql:host=$this->host;dbmane=$this->db_name", $this->user, $this->pass);
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }
    }
    public function setData($data = ""){
        if(array_key_exists("name", $data) && !empty($data)){
            $this->name = $data['name'];
        }
        if(array_key_exists("gender", $data) && !empty($data)){
            $this->gender = $data['gender'];
        }
        if(array_key_exists("id", $data) && !empty($data)){
            $this->id = $data['id'];
        }
        return $this;

    }
    public function store(){

        $statement = $this->connection->prepare("insert into atomic_project.user_gender (name, gender) values (:name, :gender)");
        $result = $statement->execute(array("name"=>$this->name, "gender"=>$this->gender));
        if($result){
            Message::message("<div class='alert alert-success'>Data has been stored successfully.</div>");
            header("location: index.php");
        }else{
            Message::message("<div class='alert alert-danger'>Data has not been stored successfully!</div>");
            header("location: create.php");
        }
    }
    public function index(){
        $statement = $this->connection->query("select * from atomic_project.user_gender where deleted_at is null");
        $all_data = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $all_data;
    }
    public function view(){
        $statement = $this->connection->prepare("select * from atomic_project.user_gender where id = :id");
        $statement->execute(array("id" => $this->id));
        $all_data = $statement->fetch(PDO::FETCH_ASSOC);
        return $all_data;
    }
}
?>