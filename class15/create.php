<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add number</title>
</head>
<body>
<form action="store.php" method="post">
    <label for="num1">First Number : </label>
    <input type="number" id="num1" name="num1" required="required" autofocus="autofocus">
    <label for="num2">Second Number : </label>
    <input type="number" id="num2" name="num2" required="required">
    <input type="submit" value="Add" name="add">
    <input type="submit" value="Subtract" name="sub">
    <input type="submit" value="Multiply" name="mul">
    <input type="submit" value="Divide" name="div">
</form>
</body>
</html>