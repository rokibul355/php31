<?php
class SubOffice{
    protected $property1 = "";
    protected $property2 = "";
    protected function set(){
        $this->property1 = "My property<br>";
        $this->property2 = "Your property<br>";
    }
    protected function get(){
        echo $this->property1;
        echo $this->property2;
    }
}
class MainOffice extends SubOffice{
    public function info(){
        $this->set();
        $this->get();
    }
}
$obj = new MainOffice();
$obj->info();
?>