<?php
class Cricket{
    public function __construct(){
        echo "I am constructor of Cricket class";
    }
}
class Movie extends Cricket{
    public function __construct(){
        echo "I am constructor of Movie class<br>";
        Cricket::__construct();
    }
}
$obj = new Movie();

?>