<?php
namespace App\Bitm\SEIP126548\Customer;
use App\Bitm\SEIP126548\Message\Message;
use PDO;
class Customer{
    public $id = "";
    public $customer_name = "";
    public $order_list = "";
    public $connection = "";
    public $host = "localhost";
    public $db_name = "labexam9";
    public $user = "root";
    public $pass = "";

    public function __construct()
    {
        try{
            $this->connection = new PDO("mysql:host=$this->host;dbname=$this->db_name", $this->user, $this->pass);
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }
    }
    public function setData($data = ""){
        if(array_key_exists("customer_name", $data) && !empty($data)){
            $this->customer_name = $data['customer_name'];
        }
        if(array_key_exists("order_list", $data) && !empty($data)){
            $this->order_list = $data['order_list'];
        }
        if(array_key_exists("id", $data) && !empty($data)){
            $this->id = $data['id'];
        }
        return $this;
    }
    public function store(){
        $statement = $this->connection->prepare("INSERT INTO labexam9.order (`customer name`, `order list`) VALUES (:name, :order)");
        $result = $statement->execute(array("name"=>$this->customer_name, "order"=>$this->order_list));
        if($result){
            Message::message("<div class='alert alert-success'>Data has been stored successfully!</div>");
            header("location: index.php");
        }else{
            Message::message("<div class='alert alert-danger'>Data has not been stored successfully!</div>");
            header("location: create.php");
        }
    }
    public function index(){
        $statement = $this->connection->query("select * from labexam9.order where deleted_at is null");
        $all_data = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $all_data;
    }
    public function delete(){
        $statement = $this->connection->prepare("delete from labexam9.order where id = :id");
        $result = $statement->execute(array("id"=>$this->id));
        if($result){
            Message::message("<div class='alert alert-success'>Data has been deleted successfully!</div>");
            header("location: index.php");
        }else{
            Message::message("<div class='alert alert-danger'>Data has not been deleted successfully!</div>");
            header("location: index.php");
        }
    }
    public function view(){
        $statement = $this->connection->prepare("select * from labexam9.order where id = :id");
        $statement->execute(array("id"=>$this->id));
        $data = $statement->fetch(PDO::FETCH_ASSOC);
        return $data;
    }

    public function trash(){
        date_default_timezone_set("Asia/Dhaka");
        $this->deleted_at = date("d-m-Y h:i:s A");
        $statement = $this->connection->prepare("update labexam9.order set deleted_at = :date where id = :id");
        $result = $statement->execute(array("date" =>$this->deleted_at, "id" =>$this->id));
        if($result){
            Message::message("<div class='alert alert-success'>Data has been trashed successfully !</div>");
            header("location: index.php");
        }else{
            Message::message("<div class='alert alert-danger'>Data has not been trashed successfully !</div>");
            header("location: index.php");
        }
    }
    public function trashView(){
        $statement = $this->connection->query("select * from labexam9.order where deleted_at is not null");
        $all_data = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $all_data;
    }
    public function restore(){
        $this->deleted_at = null;
        $statement = $this->connection->prepare("update labexam9.order set deleted_at = :date where id = :id");
        $result = $statement->execute(array("date" =>$this->deleted_at, "id" =>$this->id));
        if($result){
            Message::message("<div class='alert alert-success'>Data has been restored successfully!</div>");
            header("location: trashed.php");
        }else{
            Message::message("<div class='alert alert-danger'>Data has not been restored successfully!</div>");
            header("location: trashed.php");
        }
    }
    public function update(){
        $statement = $this->connection->prepare("update labexam9.order set `customer name` = :name, `order list` = :order where id = :id");
        $result = $statement->execute(array("name" => $this->customer_name, "order" => $this->order_list, "id" => $this->id));
        if($result){
            Message::message("<div class='alert alert-success'>Data has been updated successfully</div>");
            header("location: index.php");
        }else{
            Message::message("<div class='alert alert-danger'>Data has not been updated successfully </div>");
            header("location: index.php");
        }
    }
}
?>