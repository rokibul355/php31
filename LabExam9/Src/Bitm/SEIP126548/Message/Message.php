<?php
namespace App\Bitm\SEIP126548\Message;
if(!isset($_SESSION['message'])){
    session_start();
}
class Message{
    public static function message($message = null){
        if(is_null($message)){
            $message = self::getMessage();
            return $message;
        }else{
            self::setMessage($message);
        }
    }
    public static function setMessage($message){
        $_SESSION['message'] = $message;
    }
    public static function getMessage(){
        $message = $_SESSION['message'];
        $_SESSION['message']="";
        return $message;
    }
}
?>