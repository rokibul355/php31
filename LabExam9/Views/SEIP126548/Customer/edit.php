<?php
session_start();
include("../../../vendor/autoload.php");
use App\Bitm\SEIP126548\Customer\Customer;
use App\Bitm\SEIP126548\Message\Message;
$obj = new Customer();
$obj->setData($_GET);
$data = $obj->view();
$data['order list'] = explode(",", $data['order list']);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Edit Order</title>

    <!-- Bootstrap -->
    <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 col-md-offset-2">
            <h3>Edit Customer Order</h3>
        </div>
        <div class="col-md-2"></div>
    </div>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8" id="message">
            <?php
            if(array_key_exists("message", $_SESSION) && !empty($_SESSION)){
                echo Message::message();
            }
            ?>
        </div>
        <div class="col-md-2"></div>
    </div>
    <form action="update.php" method="post">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8 form-group">
                <label for="customer_name">Customer Name : </label>
                <input type="hidden" name="id" value="<?php echo $data['id'];?>">
                <input type="text" id="customer_name" name="customer_name" value="<?php echo $data['customer name']; ?>" class="form-control"  required="required"/>
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-3 form-group">
                <label for="burger" class="checkbox-inline"><input type="checkbox" name="order_list[]" value="Burger" id="burger" <?php if(in_array("Burger", $data['order list'])){echo "checked";}?>>Burger</label>
            </div>
            <div class="col-md-3 form-group">
                <label for="juice" class="checkbox-inline"><input type="checkbox" name="order_list[]" value="Juice" id="juice" <?php if(in_array("Juice", $data['order list'])){echo "checked";}?>>Juice</label>
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-3 form-group">
                <label for="sandwitch" class="checkbox-inline"><input type="checkbox" name="order_list[]" value="Sandwitch" id="sandwitch" <?php if(in_array("Sandwitch", $data['order list'])){echo "checked";}?>>Sandwitch</label>
            </div>
            <div class="col-md-3 form-group">
                <label for="salat" class="checkbox-inline"><input type="checkbox" name="order_list[]" value="Salat" id="salat" <?php if(in_array("Salat", $data['order list'])){echo "checked";}?>>Salat</label>
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-3 form-group">
                <label for="pizza" class="checkbox-inline"><input type="checkbox" name="order_list[]" value="Pizza" id="pizza" <?php if(in_array("Pizza", $data['order list'])){echo "checked";}?>>Pizza</label>
            </div>
            <div class="col-md-3 form-group">
                <label for="coffee" class="checkbox-inline"><input type="checkbox" name="order_list[]" value="Coffee" id="coffee" <?php if(in_array("Coffee", $data['order list'])){echo "checked";}?>>coffee</label>
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row">
            <div class="col-md-5"></div>
            <div class="col-md-3">
                <input type="submit" class="btn btn-default" value="Update Order"/>
            </div>
            <div class="col-md-4"></div>
        </div>
    </form>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
    $("#message").show().delay(3000).fadeOut();
</script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../../bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
