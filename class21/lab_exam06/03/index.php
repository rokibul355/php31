<?php
interface Manager{
    public function setPower();
    public function getPower();
}
interface Information{
    public function setInfo();
    public function getInfo();
}
interface Basis{
    public function setStudent();

    public function getStudent();
}
interface Book{
    public function readBook();
    public function writeBook();
}
class All implements Manager, Information, Basis, Book{
    public function setPower()
    {
        // TODO: Implement setPower() method.
        echo "I set the power<br>";
    }
    public function getPower()
    {
        // TODO: Implement getPower() method.
        echo "I get the power<br>";
    }
    public function setInfo()
    {
        // TODO: Implement setInfo() method.
        echo "I set Information<br>";
    }
    public function getInfo()
    {
        // TODO: Implement getInfo() method.
        echo "I get Information<br>";
    }
    public function setStudent()
    {
        // TODO: Implement setStudent() method.
        echo "I set Student<br>";
    }
    public function getStudent()
    {
        // TODO: Implement getStudent() method.
        echo "I get Student<br>";
    }
    public function readBook()
    {
        // TODO: Implement readBook() method.
        echo "I read the book<br>";
    }
    public function writeBook()
    {
        // TODO: Implement writeBook() method.
        echo "I write the book";
    }
}
All::setPower();
All::getPower();
All::setInfo();
All::getInfo();
All::setStudent();
All::getStudent();
All::readBook();
All::writeBook();
?>