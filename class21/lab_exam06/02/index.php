<?php
abstract class Manager{
    public abstract function setPower();
    public abstract function getPower();
}
abstract class Information{
    public abstract function setInfo();
    public abstract function getInfo();
}
class Assistant extends Manager{
    public function setPower()
    {
       echo "I set power<br>";
    }
    public function getPower()
    {
        echo "I get power<br>";
    }
}
class SubInfo extends Information{
    public function setInfo()
    {
        echo "I set Information<br>";
    }
    public function getInfo()
    {
        echo "I get Information<br>";
    }
}
Assistant::setPower();
Assistant::getPower();
SubInfo::setInfo();
SubInfo::getInfo();
?>