<?php
function __autoload($name){
    $array = explode("\\", $name);
    array_shift($array);
    $file = "Src/".implode("/", $array).".php";
    include_once($file);
}


use App\SEIP\S126548\Module\Users\Users;
use App\SEIP\S126548\Module\Profile\Profile;
use App\SEIP\S126548\Module\Image\Image;
use App\SEIP\S126548\Module\About\About;
use App\SEIP\S126548\Module\Contact\Contact;
$obj = new Users();
$obj2 = new Profile();
$obj3 = new Image();
$obj4 = new About();
$obj5 = new Contact();
?>